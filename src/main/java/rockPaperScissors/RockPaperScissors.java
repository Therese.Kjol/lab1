package rockPaperScissors;

import java.text.BreakIterator;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;
import javax.swing.plaf.synth.SynthStyleFactory;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random r = new Random(); 
    
    public void run() {
        while (true){
            System.out.printf("Let's play round %s. ", roundCounter);
            String mychoice = readInput("\nYour choice (Rock/Paper/Scissors)?").toLowerCase().strip(); 
            String aichoice = aichoice().toLowerCase().strip();
    
        

            if (is_winner(mychoice, aichoice) == is_winner(aichoice, mychoice)){

                System.out.println("Human chose " + mychoice + ", computer chose " + aichoice + ", It's a tie!" );
            }
            else if  (is_winner(mychoice, aichoice)){
                System.out.println("Human chose " + mychoice + ", computer chose " + aichoice + ". Human wins!");
                humanScore ++;
            } 
            else {
                System.out.println("Human chose " + mychoice +  ", computer chose " + aichoice + ". Computer wins!");
                computerScore ++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String anotherround = readInput("Do you wish to continue playing? (y/n)?");
        
            if (anotherround.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            else {
                roundCounter ++;
            }
            }
        
        

        
        

        // TODO: Implement Rock Paper Scissor
    }

    /**
     * check if player1 wins over player 2
     * @param player1
     * @param player2
     * @return true if player1 wins and false otherwise
     */
    private boolean is_winner(String player1, String player2) {
        if (player1.equals("paper")){
            return player2.equals("rock");
        }  
        else if (player1.equals("scissors")){
            return player2.equals("paper");
        }
        else {
            return player2.equals("scissors");
        }
    }

    public String aichoice(){
        int rnd = r.nextInt(rpsChoices.size());
        String result = rpsChoices.get(rnd);
        return result;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
